> {-# LANGUAGE DataKinds, DeriveFunctor, GADTs, KindSignatures, RankNTypes, StandaloneDeriving, TypeApplications, TypeOperators, ViewPatterns #-}
> module Main where
> import Data.Proxy
> import GHC.TypeNats

In the following program I will demonstrate some of the basic data types used in the Tree Automata Techniques and Applications (TATA) book

I begin with Terms, which is similar to an ordinary recursive fixpoint type but also has a compile time-tracked number of holes, which I will use to reprsent variables and to (eventually) perform substitution.

> data Term (x :: Nat) f a where
>   Mu :: (KnownNat n) => f (Term n f a) -> Term n f a
>   Var :: (KnownNat n, KnownNat m, n+1 <= m) => Proxy n -> Term m f a

> instance (Show a, Functor f, Show (f String), KnownNat x) => Show (Term x f a) where
>   show (Mu f) = show (fmap show f)
>   show (Var (natVal -> x)) = show x
> deriving instance (Functor f) => Functor (Term x f)

Example 0.0.2. Let F = {f(, , ), g(, ), h(), a, b} and X = {x, y}.

> data Example002 a =
>   F a a a | G a a | H a
>   | A | B
>   deriving (Show, Functor)

From the book: The root symbol of t is f ; the set of frontier positions of t is {11, 12, 2, 31}; the set of variable positions of t′ is {11, 12, 31, 32}; t|3 = h(b); t[a]3 = f (g(a, b), a, a); Height(t) = 3; Height(t′) = 2; ∥t∥ = 7; ∥t′∥ = 4.

> example002_t :: Term 0 Example002 ()
> example002_t =
>   Mu (F
>     (Mu (G
>       (Mu A)
>       (Mu B)))
>     (Mu A)
>     (Mu (H
>       (Mu B))))

> example002_t' :: Term 2 Example002 ()
> example002_t' =
>   Mu (F
>     (Mu (G
>       (Var (Proxy @ 0))
>       (Var (Proxy @ 1))))
>     (Mu A)
>     (Mu (G
>       (Var (Proxy @ 0))
>       (Var (Proxy @ 1)))))

> main :: IO ()
> main = do
>   putStrLn "Hello, Haskell!"
>   putStrLn (show example002_t)
>   putStrLn (show example002_t')
